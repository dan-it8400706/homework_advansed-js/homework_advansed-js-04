fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response=>response.json())
    .then(data => data.forEach(element => {
        let {id = 0,
            name = "",
            characters,
            episodeId,
            openingCrawl} = element
        draw(id,name,characters,episodeId,openingCrawl)
        
    }))
    
    function draw (id,name,characters,episodeId,openingCrawl){
        let div = document.getElementById(`${id}`)
        let title = document.createElement('h2')
        title.textContent = `Part ${episodeId}. ${name}`
        div.append(title)
        let p = document.createElement('p')
        p.textContent = openingCrawl
        div.append(p)
        let ul = document.createElement('ul')
        div.append(ul)

        characters.forEach( e => {
            let li = document.createElement('li')
            ul.append(li)

                fetch(`${e}`)
                .then(response=>response.json())
                .then(data => li.textContent = data.name)
            
        })
    }


